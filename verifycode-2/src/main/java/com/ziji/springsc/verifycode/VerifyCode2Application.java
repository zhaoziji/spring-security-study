package com.ziji.springsc.verifycode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerifyCode2Application {

    public static void main(String[] args) {
        SpringApplication.run(VerifyCode2Application.class, args);
    }

}
