package com.ziji.springsc.verifycode.config;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class MyWebAuthenticationDetails extends WebAuthenticationDetails {

    private boolean isPassed;

    public boolean isPassed() {
        return isPassed;
    }

    public MyWebAuthenticationDetails(HttpServletRequest req) {
        super(req);
        String code = req.getParameter("code");
        String verify_code = (String) req.getSession().getAttribute("verify_code");
        if (code == null || verify_code == null || !code.equals(verify_code)) {
            this.isPassed = false;
//            throw new AuthenticationServiceException("验证码错误");
        } else {
            this.isPassed = true;
        }
    }
}
