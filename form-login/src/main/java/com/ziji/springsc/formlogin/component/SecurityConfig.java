package com.ziji.springsc.formlogin.component;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
// 首先我们自定义 SecurityConfig 继承自 WebSecurityConfigurerAdapter，重写里边的 configure 方法。
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    PasswordEncoder passwordEncoder() {
        // 首先我们提供了一个 PasswordEncoder 的实例，因为目前的案例还比较简单，因此我暂时先不给密码进行加密，
        // 所以返回 NoOpPasswordEncoder 的实例即可。
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 我们通过 inMemoryAuthentication 来开启在内存中定义用户，
        // withUser 中是用户名，password 中则是用户密码，roles 中是用户角色。
        // 如果需要配置多个用户，用 and 相连。
        auth.inMemoryAuthentication()
                .withUser("ziji").password("123").roles("admin")
                .and().withUser("zeng").password("123").roles("admin");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // web.ignoring() 用来配置忽略掉的 URL 地址，一般对于静态文件，我们可以采用此操作。
        web.ignoring().antMatchers("/js/**", "/css/**","/images/**");
    }

    // 如果我们使用 XML 来配置 Spring Security ，里边会有一个重要的标签 <http>，HttpSecurity 提供的配置方法 都对应了该标签。
    // authorizeRequests 对应了 <intercept-url>。
    // formLogin 对应了 <formlogin>。
    // and 方法表示结束当前标签，上下文回到HttpSecurity，开启新一轮的配置。
    // permitAll 表示登录相关的页面/接口不要被拦截。
    // 最后记得关闭 csrf ，关于 csrf 问题我到后面专门和大家说。
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login.html")
                .permitAll()
                .and()
                .csrf().disable();
    }
}
