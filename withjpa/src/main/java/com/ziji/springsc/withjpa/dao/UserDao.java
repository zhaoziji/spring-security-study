package com.ziji.springsc.withjpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ziji.springsc.withjpa.model.User;

// 支持 jpa 操作
public interface UserDao extends JpaRepository<User,Long> {
    User findUserByUsername(String username);
}
