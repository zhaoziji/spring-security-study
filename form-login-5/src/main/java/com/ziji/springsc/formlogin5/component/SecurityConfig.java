package com.ziji.springsc.formlogin5.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;
import java.io.PrintWriter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Autowired
    private DataSource dataSource;

    @Bean
    protected UserDetailsService userDetailsService() {
        // 首先构建一个 JdbcUserDetailsManager 实例
        JdbcUserDetailsManager manager = new JdbcUserDetailsManager();
        // 给 JdbcUserDetailsManager 实例添加一个 DataSource 对象，实际调用 JdbcDaoSupport#setDataSource(dataSource)
        manager.setDataSource(this.dataSource);
        // 调用 userExists 方法判断用户是否存在，如果不存在，就创建一个新的用户出来
        // （因为每次项目启动时这段代码都会执行，所以加一个判断，避免重复创建用户）
        if (!manager.userExists("ziji")) {
            manager.createUser(User.withUsername("ziji").password("123").roles("admin").build());
        }
        if (!manager.userExists("zeng")) {
            manager.createUser(User.withUsername("zeng").password("123").roles("user").build());
        }
        return manager;
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("zijix").password("123").roles("admin")
//                .and().withUser("zengx").password("123").roles("user");
//    }

    // 角色继承，admin 包含了 user 的访问权限
    @Bean
    RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl hierarchy = new RoleHierarchyImpl();
        hierarchy.setHierarchy("ROLE_admin > ROLE_user");
        return hierarchy;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/js/**", "/css/**","/images/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 资源角色授权
                .antMatchers("/admin/**").hasRole("admin")
                .antMatchers("/user/**").hasRole("user")
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/doLogin")
                .usernameParameter("name")
                .passwordParameter("passwd")
//                .defaultSuccessUrl("/hello")
                .successHandler((req, resp, authentication) -> {
                    Object principal = authentication.getPrincipal();
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    out.write(new ObjectMapper().writeValueAsString(principal));
                    out.flush();
                    out.close();
                })
//                .failureUrl("/error_page_02")
                .failureHandler((req, resp, e) -> {
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    // 直接打印异常信息
                    out.write(e.getMessage());
                    // 将异常信息转成 json 后输出给客户端
//                    out.write(new ObjectMapper().writeValueAsString(e));
                    out.flush();
                    out.close();
                })
                .permitAll()

                .and()
                .logout()
                .logoutUrl("/doLogout")
//                .logoutSuccessUrl("/index")
                .logoutSuccessHandler((req, resp, authentication) -> {
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    out.write("注销成功");
                    out.flush();
                    out.close();
                })
                .deleteCookies()
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .permitAll()

                .and()
                .exceptionHandling()
                .authenticationEntryPoint((req, resp, authException) -> {
                            resp.setContentType("application/json;charset=utf-8");
                            PrintWriter out = resp.getWriter();
                            out.write("尚未登录，请先登录");
                            out.flush();
                            out.close();
                        }
                )

                .and()
                .csrf().disable();
    }
}
